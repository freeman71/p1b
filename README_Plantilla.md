# Backend CRUD API REST

_Ejemplo de WS REST con NodeJS que proporciona un API CRUD para gestionar una DB MongoDB._

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Ver **Deployment** para conocer cómo desplegar el proyecto.


### Pre-requisitos 📋

_Qué cosas necesitas para instalar el software y cómo instalarlas_

En primer lugar para esta practica necesitamos instalar MongoDb.

Para instalar MongoDb realizaremos una serie de pasos:
1ºsudo apt update
2ºsudo apt install -y mongodb

Es posible que tras instalar mongodb se nos lance la base de datos pero da igual ya que la iniciaremos con systemctl
1ºsudo systemctl start mongodb

Podemos ver si mongodb esta funcionando correctamente de la siguente forma:
1ºmongo --eval 'db.runCommand({ connectionStatus: 1 })'

### Instalación 🔧

_Una serie de ejemplos paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

_Indica cómo será ese paso_

Es posible que tras instalar mongodb se nos lance la base de datos pero da igual ya que la iniciaremos con systemctl
1ºsudo systemctl start mongodb

Podemos ver si mongodb esta funcionando correctamente de la siguente forma:
1ºmongo --eval 'db.runCommand({ connectionStatus: 1 })'

Ahora abriremos en otra terminal el gestor de la base de datos y probaremos algunos comando para gestionarla desde la terminal
1ºmongo --host 127.0.0.1:27017
>show dbs

Por último tenemos que instalar en nuestro proyecto la biblioteca mongodb para poder trabajar con la base de datos en nuestro caso mongojs que nos hara mas sencillo el acceso a mongo desde nuestro proyecto node:
1ºcd
2ºcd node/api-rest
3ºnpm i -S mongodb
4ºnpm i -S mongojs

Una vez tenemos esto ya podemos crear nuestro servicio web CRUD con API RESTful.

Ahora tenemos que iniciar todo.Primero pondremos en una terminal lo siguiente:
1ºsudo systemctl start mongodb

Luego en otra terminal:
1ºcd ~/node/api-rest (para abrir la carpeta api-rest)
2ºnpm start (para iniciar nuestro servicio api-rest)

Y por último en otra terminal
1ºmongo --host localhost:27017

Ahora en otra terminal abrimos visual studio code:
1ºcd ~/node/api-rest
2ºcode .

Una vez tenemos abierto el visual studio code en nuestro index.js pondremos el siguiente codigo
//
'use strict'

const port = process.env.PORT || 3000

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');

const app = express();

var db = mongojs("SD");
var id = mongojs.ObjectID;
                            
app.use(logger('dev')); 
app.use(express.urlencoded({extended: false}));
app.use(express.json()); 


app.param("coleccion", (req,res,next,coleccion) => {
    console.log('param /api/:coleccion');
    console.log('colección: ', coleccion);

    req.collection = db.collection(coleccion);
    return next();
});

app.get('/api', (req, res, next) => {
    console.log('GET /api');
    console.log(req.params);
    console.log(req.collection);

    db.getCollectionNames((err, colecciones) => {
        if (err) return next(err);
        res.json(colecciones);  
    }); 
});

app.get('/api/:coleccion', (req, res, next) => {
    req.collection.find((err, coleccion) => {
        if (err) return next(err);
        res.json(coleccion);
    });
});

app.get('/api/:coleccion/:id', (req, res, next) => {
    req.collection.findOne({_id: id(req.params.id) }, (err, elemento) => {
        if (err) return next(err);
        res.json(elemento);
    });
});

app.post('/api/:coleccion', (req, res, next) => {
    const elemento = req.body;
    if (!elemento.nombre) {
        res.status(400).json ({
            error: 'Bad data',
            description: 'Se precisa al menos un campo <nombre>'
    });
}   else {
    req.collection.save(elemento, (err, coleccionGuardada) => {
        if(err) return next(err);
    res.json(coleccionGuardada);
        });
     }
});

app.put('/api/:coleccion/:id', (req, res, next) => {
    let elementoId = req.params.id;
    let elementoNuevo = req.body;
    req.collection.update({_id: id(elementoId)},
            {$set: elementoNuevo}, {safe: true, multi: false}, (err, elementoModif) => {
        if (err) return next(err);
        res.json(elementoModif);
    });
});

app.delete('/api/:coleccion/:id', (req, res, next) => {
    let elementoId = req.params.id;
    
    req.collection.remove({_id: id(elementoId)}, (err, resultado) => {
        if (err) return next(err);
        res.json(resultado);
    });
});

app.listen(port, () => {
    console.log(`API REST ejecutándose en http://localhost:${port}/api/:coleccion/:id`);
    });
//
Asi quedaría el codigo de nuestro API RESTful

## Ejecutando las pruebas ⚙️

_Explica cómo ejecutar las pruebas automatizadas para este sistema_

Una vez tenemos nuestro codigo del API RESTful abrimos una terminal e inicamos el api-rest
1ºnpm start

Si vemos que no da ningún error abrimos el postama para ir creando,probando y guardando las rutas API que podemos ofrecer.

Primeros haremos un POST

POST http://localhost:3000/api/familia

Para que funcione correctamente tenemos que poner en la cabecera Content-type:application/json
y en el body raw y luego seleccionar json

Ahora en el body ponemos lo que queramos que aparezca al hacer POST por ejemplo:
{
	"nombre":"pedro",
	"edad":"30"
}
Cada vez que hacemos un POST o GET o lo que sea vamos guardando la respuesta.

Una vez hemos hecho el POST hacemos un GET:
http://localhost:3000/api
Con este GET obtendremos las colecciones que hayamos creado.
{
	"familia"
}
Volvemos a guardar

Hacemos otro GET
http://localhost:3000/api/familia
Con este get obtendremos el id junto a lo que hemos puesto en el POST
{
	"id":523234asfsf23fsaddf234a,
	"nombre":"pedro",
	"edad":30
}
Volvemos a guardar y hacemos un PUT que sirve para modificar o crear un nuevo campo
PUT http://localhost:3000/api/mascotas/5ad6551ff6efc76d03ddbe70
{
	"color":"marron"
}

Volvemos a guardar y hacemos otro GET en el cual veremos como se ha actualizado gracias al PUT
http://localhost:3000/api/mascotas
Con este get obtendremos lo que hay dentro de la colección mascotas en este caso junto a su id.
{
	"id":"5ad6551ff6efc76d03ddbe70",
	"nombre":"boby",
	"raza":"galgo",
	"color":marron
}
Y volvemos a guardar
	
 
### Analice las pruebas end-to-end 🔩

_Explica qué verifican estas pruebas y por qué_

Ahora utilizamos mongodb para ver que toda la información que hemos creado se encuentra en la base de datos.Para ello realizamos los siguientes pasos.

1ºAbrimos una terminal
2ºmongo
3ºshow dbs
SD	0.000GB
admin	0.000GB
config	0.000GB
local	0.000GB
4ºuse SD
switched to db SD
5ºshow collections
(nombre de las colecciones que hayamos creado)
6ºdb.nombredelacoleccion.find()

Una vez hemos probado todo y lo hemos guardado en una colección de Postma, la exportamos a un archivo JSON que copiaremos en la carpeta de nuestro proyecto(donde tenemos el index.js que hemos utilizado)

Por ultimo subimos todo al repositorio:

git add .
git commit -m "API REST CRUD con MongoDB"
git push
git tag v3.0.0
git push --tags


### Y las pruebas de estilo de codificación ⌨️

_Explica qué verifican estas pruebas y por qué_

Por último vemos la traza creada en la terminal de nuestra aplicación generada por Morgan y la sesión con el cliente mongo en la que se muestran las consultas

## Despliegue 📦



## Construido con 🛠️
MongoDB
Visual studio code
Morgan
NodeJS
Postman
## Contribuyendo 🖇️

Por favor lee el [CONTRIBUTING.md](https://gist.github.com/tu/tuProyecto) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Wiki](https://bitbucket.org/freeman71/p1b/src/master)

## Versionado 📌

Usamos [SemVer](http://semver.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://bitbucket.org/freeman71/p1b/commits)).

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Paco Maciá** - *Trabajo Inicial* - [pmacia](https://github.com/pmacia)
* **Ángel Manuel Ruiz Freeman** - *Documentación* - [freeman71](https://bitbucket.org/freeman71)

También puedes mirar la lista de todos los [contribuyentes](https://github.com/your/project/contributors) quiénes han participado en este proyecto. 

## Licencia 📄

Este proyecto está bajo la Licencia (Tu Licencia) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 o un café ☕ a alguien del equipo. 
* Da las gracias públicamente 🤓.
* etc.
